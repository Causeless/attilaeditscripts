--[[
    Copyright 2016, 2017 Tommy March

    This file is part of AE-Tools.

    AE-Tools is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    AE-Tools is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with AE-Tools.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- lib_manpower_unit_counts.lua
-- Stores table holding unit sizes

-- This is auto-generated right now. manual changes may be overwritten!

-- General
module(..., package.seeall)
_G.main_env = getfenv(1) -- Probably not needed in most places

units = {
	att_alan_alani_cavalry_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_cavalry_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_chosen_raiders = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_alan_alani_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_mounted_bows = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_mounted_sword_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_nobles_general = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_alani_swordsmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_alan_alani_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_alan_anti_cav_light_tier_3 = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_anti_cav_light_tier_4 = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_alan_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_alan_sarmatian_band = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_alan_sarmatian_cataphracts_infantry_destroyer_tier_5 = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_alan_sarmatian_shock_tier_1 = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_alan_sarmatian_shock_tier_4 = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_alan_tank = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_cel_black_blades = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_caledonian_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_cateran_brigade = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_axe_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_axe_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_bows = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_cavalry_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_celtic_crossbows = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_spears = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_warlord = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_celtic_woodrunners = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_chosen_raiders = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_cel_ebdani_cavalry_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_ebdani_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_elite_highland_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_fianna = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_followers_of_morrigan = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_gallowglass = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_gazehounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_cel_highland_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_horse_whisperers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_kerns = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_cel_kings_fianna = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_cel_kings_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_mormaer_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_mounted_spear_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_noble_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_cel_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_cel_pictish_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_pictish_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_pictish_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_round_shield_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_royal_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_cel_scathas_teachers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_cel_sighthound_spears = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_cel_tanist = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_cel_wolfhound_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_des_amazigh_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_amazigh_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_amazigh_spearmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_arabic_horsemen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_armoured_camel_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_camel_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_camel_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_desert_bowmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_chieftain = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_desert_hurlers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_legionary_defectors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_mounted_javelinmen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_desert_mounted_skirmishers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_desert_palatina_defectors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_desert_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_tribesmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_desert_warlord = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_elite_nubian_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_abunas_guard = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_adana_marksmen = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_des_es_adana_trackers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_des_es_afar_camel_riders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_afar_raidmasters = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_afar_swordsmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_al_dawser = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_al_rahrain = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_al_shahba = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_almaqahs_lancers = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_armoured_himyarite_shotelai = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_ashum = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_athars_chosen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_badyia_skirmishers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_baltha_defenders = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_baltha_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_behers_chosen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_bet_giorgis_cavalry = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_desert_pikes = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_dune_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_elite_tor_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_himyarite_shotelai = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_jamal_al_baltha = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_jamal_al_rumha = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_khahyahlim = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_kings_radif = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_es_marz_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_masqal_spearmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_mavias_bodyguards = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_mavias_chargers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_mavias_chosen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_mavias_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_mounted_marz_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_noble_al_rahrain = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_des_es_ras_guard = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_rebellion_militia = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_rumha_skirmishers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_rumha_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_sahnegohrim = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_sanai = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_sandstorm_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_sons_of_the_invincible_mahrem = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_spice_guard = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_des_es_spice_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_tanukhid_ambushers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_tanukhid_pikes = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_tor_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_wadai = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_es_zafar_sentinels = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_zealot_sicarius = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_es_zodiac_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_garamantian_hunters = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_garamantian_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_kushite_mounted_shotelai = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_des_kushite_shotelai = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_des_lancers_of_the_oasis = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_des_noble_numidian_cavalry = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_des_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_des_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_des_shawia_guard = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_des_tuareg_camel_spearmen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_armenian_slingers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_armenian_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_armoured_indian_elephants = {
		culture = "other",
		class = "rich",
		count = 24
	},

	att_est_camel_clibanarii = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_est_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_est_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_est_dailamite_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_elite_dailamite_infantry = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_elite_immortals = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_est_elite_savaran_cavalry = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_est_grivpanvar_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_gyan_avspar = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_est_immortals = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_est_indian_elephants = {
		culture = "other",
		class = "rich",
		count = 24
	},

	att_est_kurdish_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_kurdish_javelinmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_lakhmid_scouts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_naft_throwers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_est_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_est_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_est_onager_large = {
		culture = "other",
		class = "rich",
		count = 40
	},

	att_est_paighan_band = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_bowmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_bows = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_brigade = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_camel_archers = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_est_persian_camel_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_persian_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_persian_hurlers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_persian_mounted_bowmen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_persian_mounted_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_persian_nobles = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_est_persian_scouts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_persian_skirmishers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_pushtighban_cataphracts = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_est_royal_persian_archers = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_est_sassanid_onager_large = {
		culture = "other",
		class = "rich",
		count = 40
	},

	att_est_savaran_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_savaran_cavalry = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_est_savaran_sardar = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_est_sogdian_camel_raiders = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_est_sogdian_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_est_spahbed = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_est_zhayedan_immortal_cavalry = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_ger_agathyrsi_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_ger_alamannic_scavengers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_alani_general = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_ger_alani_horsemen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_ger_alani_spearmen = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_ger_antrustriones_cavalry_guard = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_axe_heerbann = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_bagaudae = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_barbed_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_barbed_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_barbed_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_bejewelled_retinue = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_burgundian_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_burgundian_guard = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_burgundian_mounted_axemen = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_burgundian_warhounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_ger_captured_cheiroballistra = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_ger_chnodomars_entourage = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_chnodomars_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_chosen_vandal_raiders = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_ger_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_ger_elite_agathyrsi_cavalry = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_ger_elite_agathyrsi_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_ger_elite_alamannic_scavengers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_elite_germanic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_elite_ostrogothic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_elite_sarmatian_cataphracts = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_ger_elite_scattershot_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_elite_sword_heerbann = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_francisca_heerbann = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_frankish_general = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	att_ger_frankish_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_frankish_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_freemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gardingi_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_germanic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_bowmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_bows = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_crossbowmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_hurlers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_mounted_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_mounted_warband = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_night_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_pikes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_germanic_spear_masters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_germanic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_godans_chosen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_godans_warlord = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_godansmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_falxmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_general = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_gothic_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_gothic_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_palatina_defectors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_gothic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_grey_hairs = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_ger_horse_slayers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_horse_stabbers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_jewelled_nobles = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_langobard_clubmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_light_moorish_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_noble_alani_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_noble_germanic_horsemen = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	att_ger_noble_germanic_swordsmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_noble_gothic_lancers = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	att_ger_noble_suebi_swordsmen = {
		culture = "barbarian",
		class = "rich",
		count = 160
	},

	att_ger_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_ger_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_ger_onager_large = {
		culture = "barbarian",
		class = "rich",
		count = 40
	},

	att_ger_protectores_defectors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_redoubtable_chieftain = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_royal_anstrutiones = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_royal_burgundian_general = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_royal_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_royal_saiones = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	att_ger_royal_suebi_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_sacra_francisca = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_salian_frankish_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_sarmatian_cataphract_archers = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_ger_sarmatian_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_ger_sarmatian_mounted_skirmishers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_ger_sarmatian_spears = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_ger_sarmatian_warband = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_ger_scaled_clubmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_scattershot_hurlers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_scavenger_mob = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_suebi_champions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_suebi_nobles = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_ger_suebi_oath_takers = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_suebi_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_sword_heerbann = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_taifali_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_thracian_oathsworns = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_ger_thracian_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_vandal_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_vandal_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_vandal_warlord = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_ger_visigothic_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_ger_young_wolves = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_mar_ger_elite_vandal_archer_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_mar_rom_byzantine_light_marines = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_mar_rom_byzantine_marines = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_merc_cel_celtic_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_axe_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_cavalry_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_cel_celtic_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_spears = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_celtic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_cel_gazehounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_merc_cel_mounted_spear_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_cel_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_merc_des_camel_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_des_desert_bowmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_des_desert_hurlers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_des_desert_legionary_defectors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_des_desert_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_des_desert_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_des_elite_nubian_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_des_garamantian_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_des_onager = {
		culture = "other",
		class = "rich",
		count = 40
	},

	att_merc_des_tuareg_camel_spearmen = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_merc_est_armenian_slingers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_armenian_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_dailamite_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_kurdish_archers = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_merc_est_kurdish_javelinmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_lakhmid_scouts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_est_onager = {
		culture = "other",
		class = "rich",
		count = 40
	},

	att_merc_est_persian_brigade = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_persian_hurlers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_persian_mounted_bowmen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_est_persian_mounted_warriors = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_est_persian_scouts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_est_persian_skirmishers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_est_savaran_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_est_savaran_cavalry = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_merc_ger_agathyrsi_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_ger_alani_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_ger_axe_heerbann = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_merc_ger_frankish_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_ger_germanic_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_germanic_hurlers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_germanic_mounted_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_ger_germanic_mounted_warband = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_ger_germanic_pikes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_germanic_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_gothic_falxmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_gothic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_light_moorish_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_ger_onager = {
		culture = "barbarian",
		class = "rich",
		count = 40
	},

	att_merc_ger_sarmatian_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_ger_thracian_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_ger_vandal_raiders = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_merc_nom_bosphoran_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_merc_nom_hunnic_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_nom_hunnic_mounted_warband = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_nom_onager = {
		culture = "other",
		class = "rich",
		count = 40
	},

	att_merc_nom_steppe_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_nom_steppe_mounted_bows = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_nom_steppe_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_merc_nor_nordic_axe_warband = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_merc_nor_nordic_brigade = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_nor_nordic_mounted_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_nor_nordic_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_nor_onager = {
		culture = "barbarian",
		class = "rich",
		count = 40
	},

	att_merc_nor_saxon_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_merc_nor_saxon_spears = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_nor_thrall_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_nor_wolf_coats = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_merc_rom_ballistarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_merc_rom_comitatensis_spears = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_merc_rom_contarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_merc_rom_equites_dalmatae = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_merc_rom_funditores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_merc_rom_legio = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_merc_rom_onager = {
		culture = "roman",
		class = "rich",
		count = 40
	},

	att_merc_rom_palatina = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_merc_rom_sagittarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_nom_avar_horde = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nom_bosphoran_infantry = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_bosphoran_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_chosen_uar_warriors = {
		culture = "other",
		class = "med",
		count = 160
	},

	att_nom_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_nom_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_nom_elite_hunnic_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_first_wave_lancers = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nom_hunnic_ambushers = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nom_hunnic_devil_archers = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nom_hunnic_devil_cavalry = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_dismounted_warband = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_hunnic_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_horsemen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_mounted_bows = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_mounted_warband = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_hunnic_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_hunnic_warlord = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_nom_noble_acatziri_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_noble_horse_archers = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_nom_noble_steppe_cataphracts = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_nom_nokkors = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nom_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_nom_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_nom_onager_large = {
		culture = "other",
		class = "med",
		count = 40
	},

	att_nom_scirii_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_shamans_of_the_eternal_sky = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_bows = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_steppe_cataphracts = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_chieftain = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_steppe_mounted_bows = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_mounted_brigands = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_mounted_tribespeople = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_shield_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_steppe_spearmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_steppe_tribespeople = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_steppe_warlord = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_nom_steppe_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_uar_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_uar_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_nom_unnigarde = {
		culture = "other",
		class = "med",
		count = 80
	},

	att_nor_chieftain_with_gedriht = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_nor_chosen_warriors = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_nor_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_nor_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_nor_elite_duguth_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_elite_nordic_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_elite_nordic_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_elite_saxon_javelinmen = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_nor_geoguth_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_gesithas = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_nor_hearth_guard = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_heroic_cavalry = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	att_nor_hirdmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_huscarls = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_axe_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_axe_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_bows = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_brigade = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_brigands = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_horse_lords = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_nordic_horse_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_nordic_hurlers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_mounted_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_nordic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_raid_leader = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_nordic_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_spear_masters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_warlord = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_nordic_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_norse_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_nor_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_nor_onager_large = {
		culture = "barbarian",
		class = "rich",
		count = 40
	},

	att_nor_royal_huscarls = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_nor_saxon_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_saxon_mounted_warband = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_saxon_spears = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_thrall_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_thrall_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_thrall_spears = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_viking_captain = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_viking_raider_cavalry = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_nor_viking_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_nor_warhounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_nor_wolf_coats = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_rom_armigeri_defensores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_armoured_sagittarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_balistarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_ballistarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_catafractarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_cheiroballistra = {
		culture = "roman",
		class = "poor",
		count = 40
	},

	att_rom_civilian_farmers = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_rom_civilian_townsfolk = {
		culture = "other",
		class = "poor",
		count = 40
	},

	att_rom_clibanarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_cohors = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_comes = {
		culture = "roman",
		class = "med",
		count = 80
	},

	att_rom_comitatensis_spears = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_contarii = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_rom_cornuti_seniores = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_eastern_armoured_legio = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_eastern_auxilia_palatina = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_elite_ballistarii = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_elite_palatina = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_equites_dalmatae = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_rom_equites_promoti = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_equites_sagittarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_excubitores_cavalry_guard = {
		culture = "roman",
		class = "med",
		count = 80
	},

	att_rom_exculcatores = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_exploratores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_foederati_spears = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_funditores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_herculiani_seniores = {
		culture = "roman",
		class = "med",
		count = 160
	},

	att_rom_hetaireia_guards = {
		culture = "roman",
		class = "rich",
		count = 160
	},

	att_rom_imperial_dromedarii = {
		culture = "roman",
		class = "rich",
		count = 80
	},

	att_rom_lanciarii_seniores = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_legio = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_legio_comitatenses = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_levis_armaturae = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_limitanei_borderguards = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_magister_militum = {
		culture = "roman",
		class = "rich",
		count = 80
	},

	att_rom_matiarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_menaulatoi = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_noble_foederati_javelinmen = {
		culture = "roman",
		class = "rich",
		count = 160
	},

	att_rom_numeroi = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	att_rom_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_rom_onager_large = {
		culture = "roman",
		class = "rich",
		count = 40
	},

	att_rom_palatina = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_palatina_guards = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	att_rom_praeventores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_protectores_domestici = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_ravenna_elite_ballistarii = {
		culture = "roman",
		class = "med",
		count = 160
	},

	att_rom_sagittarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	att_rom_scholae_gentiles = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_scholae_palatinae = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	att_rom_scout_equites = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_rom_tagmata_cavalry = {
		culture = "roman",
		class = "med",
		count = 80
	},

	att_rom_western_auxilia_palatina = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_shp_alan_archers_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_alan_artillery_dbl_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_alan_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_alan_boatment_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_alan_boatment_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_alan_boatment_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_alan_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_alan_bowmen_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 96
	},

	att_shp_alan_chosen_archers_dro_heavy = {
		culture = "barbarian",
		class = "med",
		count = 156
	},

	att_shp_alan_chosen_boatmen_lib_heavy_ft = {
		culture = "barbarian",
		class = "rich",
		count = 140
	},

	att_shp_alan_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_alan_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_cel_artillery_dbl_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_cel_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_cel_berserker_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_cel_boatmen_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 32
	},

	att_shp_cel_boatmen_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_cel_boatmen_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_cel_celtic_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_cel_chosen_celtic_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 132
	},

	att_shp_cel_ebdani_heavy_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_cel_elite_highland_bowman_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_cel_elite_norse_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 132
	},

	att_shp_cel_heavy_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_cel_highland_archer_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_cel_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_cel_marauders_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_cel_pictish_berserker_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 132
	},

	att_shp_cel_transport_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 124
	},

	att_shp_cel_transport_light = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_des_assault_corsairs_dro_light_fs = {
		culture = "other",
		class = "poor",
		count = 32
	},

	att_shp_des_assault_corsairs_lib_light_ram = {
		culture = "other",
		class = "poor",
		count = 20
	},

	att_shp_des_assault_corsairs_lib_light_spike = {
		culture = "other",
		class = "poor",
		count = 36
	},

	att_shp_des_corsairs_dro_light = {
		culture = "other",
		class = "poor",
		count = 72
	},

	att_shp_des_heavy_corsairs_lib_light = {
		culture = "other",
		class = "poor",
		count = 72
	},

	att_shp_des_naval_bowmen_dro_light = {
		culture = "other",
		class = "poor",
		count = 88
	},

	att_shp_eas_artillery_dbl_dro_heavy = {
		culture = "other",
		class = "poor",
		count = 64
	},

	att_shp_eas_artillery_dro_heavy = {
		culture = "other",
		class = "poor",
		count = 64
	},

	att_shp_eas_artillery_lib_light = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_eas_boatmen_dro_heavy = {
		culture = "other",
		class = "poor",
		count = 140
	},

	att_shp_eas_hurlers_dro_light = {
		culture = "other",
		class = "poor",
		count = 60
	},

	att_shp_eas_light_boatmen_dro_light = {
		culture = "other",
		class = "poor",
		count = 68
	},

	att_shp_er_archers_dro_light = {
		culture = "barbarian",
		class = "poor",
		count = 92
	},

	att_shp_er_archers_dro_light_sco = {
		culture = "barbarian",
		class = "poor",
		count = 92
	},

	att_shp_er_elite_marines_dro_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 140
	},

	att_shp_er_elite_marines_dro_heavy_fs = {
		culture = "barbarian",
		class = "med",
		count = 140
	},

	att_shp_er_elite_marines_dro_heavy_ft = {
		culture = "barbarian",
		class = "med",
		count = 140
	},

	att_shp_er_light_marines_lib_heavy = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	att_shp_er_marines_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_shp_fra_marines_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_ger_archers_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_ger_artillery_dbl_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_ger_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_ger_boatment_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_ger_boatment_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_ger_boatment_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_ger_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_ger_bowmen_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 96
	},

	att_shp_ger_chosen_archers_dro_heavy = {
		culture = "barbarian",
		class = "med",
		count = 156
	},

	att_shp_ger_chosen_boatmen_lib_heavy_ft = {
		culture = "barbarian",
		class = "rich",
		count = 140
	},

	att_shp_ger_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_ger_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_got_marines_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_merc_cel_berserker_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_merc_cel_celtic_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_merc_cel_heavy_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_merc_cel_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_cel_marauders_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_merc_des_corsairs_dro_light = {
		culture = "other",
		class = "poor",
		count = 72
	},

	att_shp_merc_des_heavy_corsairs_lib_light = {
		culture = "other",
		class = "poor",
		count = 72
	},

	att_shp_merc_eas_artillery_lib_light = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_merc_eas_hurlers_dro_light = {
		culture = "other",
		class = "poor",
		count = 60
	},

	att_shp_merc_eas_light_boatmen_dro_light = {
		culture = "other",
		class = "poor",
		count = 68
	},

	att_shp_merc_ger_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_ger_boatment_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_merc_ger_boatment_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_merc_ger_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_ger_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_nor_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_nor_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_merc_rom_artillery_lib_light = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_merc_rom_light_marines_lib_light = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	att_shp_merc_rom_skirmisher_lib_light = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	att_shp_merc_sax_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 92
	},

	att_shp_merc_van_artillery_lib_light = {
		culture = "other",
		class = "poor",
		count = 64
	},

	att_shp_merc_van_boatmen_lib_light_ram = {
		culture = "other",
		class = "poor",
		count = 20
	},

	att_shp_merc_van_bow_lon_light_dagger = {
		culture = "other",
		class = "poor",
		count = 88
	},

	att_shp_merc_van_light_marauders_lon_light = {
		culture = "other",
		class = "poor",
		count = 64
	},

	att_shp_merc_vik_norse_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_nor_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_nor_berserker_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_nor_boatmen_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 32
	},

	att_shp_nor_boatmen_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_nor_boatmen_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_nor_heavy_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_nor_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_nor_marauders_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_nor_transport_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 124
	},

	att_shp_nor_transport_light = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_ost_archers_dro_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 92
	},

	att_shp_ost_elite_archers_lib_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 92
	},

	att_shp_rom_artillery_dbl_lib_heavy = {
		culture = "roman",
		class = "med",
		count = 64
	},

	att_shp_rom_artillery_dbl_lib_light = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_rom_artillery_lib_heavy = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_rom_artillery_lib_light = {
		culture = "roman",
		class = "poor",
		count = 64
	},

	att_shp_rom_bowmen_lib_light = {
		culture = "roman",
		class = "slave",
		count = 96
	},

	att_shp_rom_heavy_marines_lib_heavy = {
		culture = "roman",
		class = "poor",
		count = 148
	},

	att_shp_rom_light_marines_lib_light = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	att_shp_rom_skirmisher_lib_light = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	att_shp_rom_transport_heavy = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	att_shp_rom_transport_light = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	att_shp_sas_archer_dro_light = {
		culture = "other",
		class = "poor",
		count = 92
	},

	att_shp_sas_bowman_dro_light = {
		culture = "other",
		class = "poor",
		count = 92
	},

	att_shp_sas_elite_archer_dro_heavy_dbl_sco = {
		culture = "other",
		class = "rich",
		count = 156
	},

	att_shp_sas_elite_archer_dro_heavy_sco = {
		culture = "other",
		class = "med",
		count = 156
	},

	att_shp_sas_elite_heavy_marines_dro_heavy_ft = {
		culture = "other",
		class = "rich",
		count = 140
	},

	att_shp_sas_elite_light_marines_dro_heavy_fs = {
		culture = "other",
		class = "poor",
		count = 140
	},

	att_shp_sas_light_marines_dro_light_fs = {
		culture = "other",
		class = "poor",
		count = 36
	},

	att_shp_sas_naft_marines_dro_light = {
		culture = "other",
		class = "poor",
		count = 60
	},

	att_shp_sas_slinger_dro_light = {
		culture = "other",
		class = "poor",
		count = 92
	},

	att_shp_sax_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_sax_elite_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 124
	},

	att_shp_sax_royal_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 124
	},

	att_shp_slav_archers_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_slav_artillery_dbl_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_slav_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_slav_boatment_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_slav_boatment_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_slav_boatment_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_shp_slav_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_slav_bowmen_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 96
	},

	att_shp_slav_chosen_archers_dro_heavy = {
		culture = "barbarian",
		class = "med",
		count = 156
	},

	att_shp_slav_chosen_boatmen_lib_heavy_ft = {
		culture = "barbarian",
		class = "rich",
		count = 140
	},

	att_shp_slav_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 56
	},

	att_shp_slav_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 72
	},

	att_shp_van_artillery_dbl_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_van_artillery_lib_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_van_artillery_lib_light = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_van_boatmen_dro_light_fs = {
		culture = "barbarian",
		class = "poor",
		count = 48
	},

	att_shp_van_boatmen_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_van_bow_lon_light_dagger = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	att_shp_van_bowmen_lon_light_sword = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	att_shp_van_chosen_marauders_lib_heavy_dbl_ft = {
		culture = "barbarian",
		class = "rich",
		count = 148
	},

	att_shp_van_chosen_marauders_lib_heavy_ft = {
		culture = "barbarian",
		class = "rich",
		count = 148
	},

	att_shp_van_elite_archers_dro_light_dbl_sco = {
		culture = "barbarian",
		class = "poor",
		count = 96
	},

	att_shp_van_elite_archers_dro_light_sco = {
		culture = "barbarian",
		class = "poor",
		count = 96
	},

	att_shp_van_elite_marauders_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_van_heavy_marauders_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	att_shp_van_light_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 64
	},

	att_shp_van_marauders_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_shp_vik_chosen_norse_lon_heavy = {
		culture = "barbarian",
		class = "med",
		count = 132
	},

	att_shp_vik_elite_norse_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 132
	},

	att_shp_vik_norse_bow_lon_light = {
		culture = "barbarian",
		class = "poor",
		count = 84
	},

	att_shp_vis_elite_crossbow_lib_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 148
	},

	att_shp_vis_elite_marines_lon_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 132
	},

	att_shp_wr_ballista_lib_heavy = {
		culture = "barbarian",
		class = "poor",
		count = 156
	},

	att_shp_wr_ballista_lib_heavy_dbl_sco = {
		culture = "barbarian",
		class = "poor",
		count = 156
	},

	att_shp_wr_ballista_lib_heavy_sco = {
		culture = "barbarian",
		class = "poor",
		count = 156
	},

	att_shp_wr_heavy_marines_lib_heavy_ft = {
		culture = "barbarian",
		class = "rich",
		count = 148
	},

	att_shp_wr_light_marines_lib_light_ram = {
		culture = "barbarian",
		class = "poor",
		count = 20
	},

	att_shp_wr_light_marines_lib_light_spike = {
		culture = "barbarian",
		class = "poor",
		count = 36
	},

	att_slav_ambushers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_axe_levy = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_axe_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_champions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_chosen_shields = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_followers_veles = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_horse_butchers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_horse_cutters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_horse_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_slav_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_large_shields = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_levy_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_levy_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_levy_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_noble_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_noble_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	att_slav_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	att_slav_peruns_axes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_peruns_champions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_posion_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_posion_hunters = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_slav_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_slav_skirmishers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_svarogs_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	att_slav_veles_chosen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_slav_warlords_guard = {
		culture = "barbarian",
		class = "med",
		count = 160
	},

	att_slav_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	att_wh_guardians_of_the_hindu_kush = {
		culture = "other",
		class = "rich",
		count = 160
	},

	att_wh_hephthalite_chargers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_wh_khingilas_khandas = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_wh_kindred_of_the_sun = {
		culture = "other",
		class = "rich",
		count = 80
	},

	att_wh_spet_xyon_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	att_wh_xionite_spears = {
		culture = "other",
		class = "poor",
		count = 160
	},

	att_wh_yanda_spearmasters = {
		culture = "other",
		class = "med",
		count = 160
	},

	bel_ost_ballistarii = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_cheiroballista = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	bel_ost_comes = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	bel_ost_domestici = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_elite_ostrogothic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_elite_sagittarii = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_gothic_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	bel_ost_gothic_lancers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	bel_ost_gothic_pikemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_iuvenes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_large_onager = {
		culture = "barbarian",
		class = "rich",
		count = 40
	},

	bel_ost_milites = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_milites_comitatenses = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_noble_gothic_landers = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	bel_ost_noble_horsemen = {
		culture = "barbarian",
		class = "med",
		count = 80
	},

	bel_ost_noble_javelinmen = {
		culture = "barbarian",
		class = "rich",
		count = 160
	},

	bel_ost_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	bel_ost_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	bel_ost_sagittarii = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_scholae = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	bel_ost_spear_milites = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_spear_milites_comitatenses = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_spear_veterans = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_ost_veterans = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	bel_rom_antesignani = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	bel_rom_armoured_sagittarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_ballistarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_bucellarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	bel_rom_bucellarii_guard_axemen = {
		culture = "roman",
		class = "med",
		count = 160
	},

	bel_rom_bucellarii_guard_cavalry = {
		culture = "roman",
		class = "med",
		count = 80
	},

	bel_rom_cataphracti = {
		culture = "roman",
		class = "med",
		count = 80
	},

	bel_rom_cheiroballista = {
		culture = "roman",
		class = "poor",
		count = 40
	},

	bel_rom_clibanarii = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	bel_rom_elite_ballistarii = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	bel_rom_equites_promoti = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	bel_rom_exploratores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_foederati = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_foederati_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	bel_rom_foederati_horse_archers = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	bel_rom_funditores = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_hippo_toxotai = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	bel_rom_imperial_dromedarii = {
		culture = "roman",
		class = "rich",
		count = 80
	},

	bel_rom_large_onager = {
		culture = "roman",
		class = "rich",
		count = 40
	},

	bel_rom_levis_armaturae = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_limitanei = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	bel_rom_onager_bastion = {
		culture = "roman",
		class = "slave",
		count = 4
	},

	bel_rom_praeventores = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	bel_rom_sagittarii = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_scout_equites = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	bel_rom_skutatoi = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_skutatoi_axemen = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	bel_rom_skutatoi_spears = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	bel_rom_tagmata_cavalry = {
		culture = "roman",
		class = "med",
		count = 80
	},

	bel_van_moorish_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	bel_van_noble_vandal_horsemen = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	bel_van_noble_vandal_swordsmen = {
		culture = "barbarian",
		class = "rich",
		count = 160
	},

	bel_van_vandal_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	bel_van_vandali_comitatus = {
		culture = "barbarian",
		class = "rich",
		count = 80
	},

	cha_avar_dragonship_guard_marines = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_avar_foot_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_avar_guard = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_horse_masters = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_levy_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_avar_levy_spear = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_avar_longship_boatmen = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_avar_longship_heavy_boatmen = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_avar_longship_light_boatmen = {
		culture = "other",
		class = "poor",
		count = 88
	},

	cha_avar_noble_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_noble_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_avar_noble_sword = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_avar_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	cha_avar_onager_bastion = {
		culture = "other",
		class = "poor",
		count = 4
	},

	cha_avar_spear = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_avar_tribesmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_bre_armorican_cav = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_bre_armorican_knights = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_byz_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_armoured_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_artillery_dromon = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	cha_byz_castle_dromon_guard_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_byz_dromon_archer_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_byz_dromon_levy_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_byz_dromon_skutatoi = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_byz_duxs_guard = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_byz_iuvenes = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_large_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_byz_levy_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_light_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_byz_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_byz_onager_bastion = {
		culture = "roman",
		class = "slave",
		count = 4
	},

	cha_byz_scout_cavalry = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_byz_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_skutatoi = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_skutatoi_axemen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_skutatoi_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_byz_tagmata_cavalry = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_cel_armoured_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_currach_teulu_marines = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_dragonship_teulu_marines = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_fianna = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_gazelhounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_cel_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_cel_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_kerns = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_kings_fianna = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_cel_kings_warband = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_levy_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_levy_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_long_fhada__sword_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_long_fhada_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_cel_long_fhada_javelin_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_cel_long_fhada_spear_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_longship_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_cel_longship_javelin_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_cel_longship_spear_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_longship_sword_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_cel_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_cel_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	cha_cel_pictish_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_pictish_crossbowmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_pictish_nobles = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_cel_royal_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_cel_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_sword = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_sword_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_sword_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_teulu = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_cel_welsh_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_cel_welsh_longbowmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_dragonship_royal_marines = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_eng_fyrd_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_fyrd_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_fyrd_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_fyrd_slingers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_fyrd_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_eng_longship_fyrd_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_eng_longship_fyrd_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_eng_longship_thegn_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_eng_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_eng_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	cha_eng_royal_companions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_royal_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_eng_royal_thegns = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_select_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_select_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_select_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_thegn_spearmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_eng_thegns = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_fra_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_armoured_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_armoured_sword = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_artillery_dromon = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	cha_fra_castle_dromon_picked_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_fra_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_fra_dromon_heavy_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_fra_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_fra_dromon_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_fra_guardsmen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_fra_horsemen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_fra_large_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_fra_levy_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_levy_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_levy_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_mounted_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_mounted_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_mounted_swordsmen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_fra_onager_bastion = {
		culture = "roman",
		class = "slave",
		count = 4
	},

	cha_fra_palace_guard = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_fra_paladin_guardsmen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_fra_scola_cav = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_fra_scola_knights = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_fra_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_fra_sword = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_armoured_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_armoured_sword = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_artillery_dromon = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	cha_gen_castle_dromon_guard_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_gen_dromon_heavy_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_gen_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_gen_dromon_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_gen_guard_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_gen_knights = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_gen_large_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_gen_levy_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_levy_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_levy_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_noble_cav = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_gen_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_gen_onager_bastion = {
		culture = "roman",
		class = "slave",
		count = 4
	},

	cha_gen_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_skirm_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_gen_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_gen_sword = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_artillery_dromon = {
		culture = "roman",
		class = "slave",
		count = 64
	},

	cha_lom_axemen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_castle_dromon_gastald_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_lom_clubmen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_dromon_heavy_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_lom_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_lom_dromon_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_lom_gastald_cav = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_lom_gastald_guardsmen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_lom_gastald_knights = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_lom_gastald_swordsmen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_guardsmen = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_lom_horsemen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_lom_lancers = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_lom_large_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_lom_levy = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_levy_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_levy_axemen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_levy_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_levy_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_night_raiders = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_lom_onager = {
		culture = "roman",
		class = "slave",
		count = 40
	},

	cha_lom_onager_bastion = {
		culture = "roman",
		class = "slave",
		count = 4
	},

	cha_lom_raider_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_lom_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_lom_sword = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_avar_horse_archers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_merc_avar_lancers = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_merc_avar_longship_boatmen = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_merc_avar_longship_light_boatmen = {
		culture = "other",
		class = "poor",
		count = 88
	},

	cha_merc_bre_armorican_cav = {
		culture = "roman",
		class = "med",
		count = 80
	},

	cha_merc_byz_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_byz_dromon_archer_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_merc_byz_dromon_levy_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_merc_byz_dromon_skutatoi = {
		culture = "roman",
		class = "poor",
		count = 128
	},

	cha_merc_byz_light_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_byz_skutatoi = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_merc_cel_armoured_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_cel_fianna = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_cel_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_merc_cel_kerns = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_cel_long_fhada_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_cel_long_fhada_javelin_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_cel_long_fhada_spear_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_cel_longship_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_cel_longship_javelin_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_cel_longship_spear_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_cel_pictish_crossbowmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_cel_sword_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_cel_welsh_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_eng_fyrd_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_eng_fyrd_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_eng_horsemen = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_merc_eng_longship_fyrd_archer_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_eng_longship_fyrd_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_eng_thegns = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_fra_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_fra_armoured_sword = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_merc_fra_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_merc_fra_dromon_marines = {
		culture = "roman",
		class = "poor",
		count = 128
	},

	cha_merc_fra_horsemen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_fra_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_gen_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_gen_armoured_spear = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_merc_gen_armoured_sword = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_merc_gen_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_merc_gen_dromon_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_merc_gen_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_gen_skirm_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_gen_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_lom_archers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_lom_clubmen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_lom_dromon_light_boatmen = {
		culture = "roman",
		class = "slave",
		count = 88
	},

	cha_merc_lom_dromon_marines = {
		culture = "roman",
		class = "slave",
		count = 128
	},

	cha_merc_lom_horsemen = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_lom_raider_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_lom_spear = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_mus_andalusian_cav = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_merc_mus_andalusian_infantry = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_merc_mus_andalusian_skirm = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_merc_mus_berber_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_merc_mus_berber_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_merc_mus_berber_spearmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_merc_mus_dromonian_galley_andalusian_marines = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_merc_mus_dromonian_galley_archer_boatmen = {
		culture = "other",
		class = "poor",
		count = 88
	},

	cha_merc_sax_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_sax_longship_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_sax_longship_light_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_sax_longship_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_sax_scout_cav = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_merc_sax_seax_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_sax_spear_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_slav_ambushers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_slav_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_slav_longship_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_slav_longship_light_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_slav_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_merc_slav_tribesmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_spa_ambushers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_merc_spa_iberian_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_spa_raider_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_merc_vik_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_vik_axe_freemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_vik_big_axes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_merc_vik_longship_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_vik_longship_ship_archers = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_merc_vik_longship_smashers = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_merc_vik_sword_hirdmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_mus_andalusian_cav = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_andalusian_infantry = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_andalusian_skirm = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_andalusian_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_armoured_archers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_armoured_cav = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_artillery_dromon = {
		culture = "other",
		class = "poor",
		count = 64
	},

	cha_mus_berber_horsemen = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_berber_jinetes = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_berber_levy = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_berber_light_cav = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_berber_raiders = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_berber_spearmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_berber_warriors = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_castle_dromon_guard_marines = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_mus_dromon_archer_marines = {
		culture = "other",
		class = "poor",
		count = 88
	},

	cha_mus_dromon_armoured_marines = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_mus_dromonian_galley_andalusian_marines = {
		culture = "other",
		class = "poor",
		count = 128
	},

	cha_mus_dromonian_galley_archer_boatmen = {
		culture = "other",
		class = "poor",
		count = 88
	},

	cha_mus_large_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	cha_mus_onager = {
		culture = "other",
		class = "poor",
		count = 40
	},

	cha_mus_onager_bastion = {
		culture = "other",
		class = "poor",
		count = 4
	},

	cha_mus_slingers = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_mus_umayyad_guard_cav = {
		culture = "other",
		class = "poor",
		count = 80
	},

	cha_mus_umayyad_guardsmen = {
		culture = "other",
		class = "poor",
		count = 160
	},

	cha_pap_guardsmen = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_sax_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_axe_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_axe_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_cav = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_sax_dragonship_hearth_marines = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_sax_hearth_cav = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_sax_hearth_guard = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_levy_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_levy_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_longship_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_sax_longship_heavy_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_sax_longship_light_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_sax_longship_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_sax_noble_javelinmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_sax_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	cha_sax_scout_cav = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_sax_seax_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_seax_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_spear_band = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_spear_veterans = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_spear_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_sword_veterans = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_sax_warhounds = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_slav_ambushers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_champions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_dragonship_elite_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_slav_large_shields = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_longship_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_slav_longship_light_boatmen = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_slav_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_slav_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	cha_slav_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_slav_royal_cav = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_slav_skirm = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_tribesmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_slav_warriors = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_spa_ambushers = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_spa_iberian_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_spa_jinetes = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_spa_noble_skirm = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_spa_raider_cav = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_spa_royal_cav = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_spa_royal_guard_cavalry = {
		culture = "roman",
		class = "slave",
		count = 80
	},

	cha_spa_royal_guardsmen = {
		culture = "roman",
		class = "poor",
		count = 160
	},

	cha_spa_royal_knights = {
		culture = "roman",
		class = "poor",
		count = 80
	},

	cha_spa_tribesmen = {
		culture = "roman",
		class = "slave",
		count = 160
	},

	cha_vik_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_armoured_archers = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_axe_freemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_axe_hirdmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_axemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_berserkers = {
		culture = "barbarian",
		class = "poor",
		count = 80
	},

	cha_vik_big_axes = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_dragonship_housecarl = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_dragonship_warlords_companions = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_housecarls = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_hunters = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_levy_freemen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_longship_heavy_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_longship_marauders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_longship_raiders = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_longship_ship_archers = {
		culture = "barbarian",
		class = "poor",
		count = 88
	},

	cha_vik_longship_smashers = {
		culture = "barbarian",
		class = "poor",
		count = 128
	},

	cha_vik_onager = {
		culture = "barbarian",
		class = "poor",
		count = 40
	},

	cha_vik_onager_bastion = {
		culture = "barbarian",
		class = "poor",
		count = 4
	},

	cha_vik_skirm = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_spear = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_sword_hirdmen = {
		culture = "barbarian",
		class = "poor",
		count = 160
	},

	cha_vik_warlords_companions = {
		culture = "barbarian",
		class = "poor",
		count = 160
	}
}